#folders=(lal lalframe lalmetaio lalsimulation lalburst lalinspiral lalinference lalapps)
dir=/home/chun-lung.chan/.conda/envs/numerical-mass-model-230608/
folders=(lalsimulation)
for folder in "${folders[@]}" ; do echo ${folder} ; cd ${folder} ; ./00boot ; ./configure --enable-gcc-flags=no --enable-swig-python --prefix ${dir} ; make -j  8 ; make install -j 8 ; cd ../ ; done
