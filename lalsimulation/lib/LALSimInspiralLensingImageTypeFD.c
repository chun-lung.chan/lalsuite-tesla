/*
 * Copyright (C) 2015 M. Haney, A. Gopakumar
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with with program; see the file COPYING. If not, write to the
 *  Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *  MA  02110-1301  USA
 */

#include <math.h>

#include <gsl/gsl_const.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_odeiv.h>
#include <lal/LALSimInspiralLensingImageTypeFD.h>
#include <lal/FrequencySeries.h>
#include <lal/LALSimInspiral.h>
#include <lal/LALConstants.h>
#include <lal/LALStdlib.h>
#include <lal/TimeFreqFFT.h>
#include <lal/TimeSeries.h>
#include <lal/Units.h>
#include <lal/XLALError.h>
#include <lal/LALSimInspiralWaveformParams.h>
#include <complex.h>
#include <pthread.h>


#include "check_series_macros.h"

#ifdef __GNUC__
#define UNUSED __attribute__ ((unused))
#else
#define UNUSED
#endif

pthread_mutex_t file_mutex_FD = PTHREAD_MUTEX_INITIALIZER; // Global mutex

int get_num_rows_FD(const char *filename){
	FILE *fp = fopen(filename, "r");
	char c;
	long long int count = 0;
	//printf("count in FD count \n") ;
	for (c = getc(fp); c != EOF; c = getc(fp)){
		if (c == '\n'){
			count = count + 1;
		}
	}
	fclose(fp);
	//printf("count in FD count\n");
	return count;
}

//Function for reading the amplification factor array
//array is being read at each wavoform frequency instead of reading the whole array
//in order to avoid overflow issues in C and HTCondor 
void XLALSimInspiralLensingAmplificationFactorReaderFD(const char *filename, 
						int target_line,
						long double *LensingAmplificationFactorArraySamplingFrequencies,
						long double *LensingAmplificationFactorReal, 
						long double *LensingAmplificationFactorComplex){

	// Obtain the mutex before file operations
	pthread_mutex_lock(&file_mutex_FD);

	FILE *fp = fopen(filename, "r");
	long double val;
	char row[77]; //numer of characters of the target line
	char *current_ptr;

	//Calcuclate the position of the line
	//Each line consist 77 characters
	//Please make sure the lensing amplfication array satisify:
	//deltaf 1.5625e-2
	//fmin = 0, fmax = 8192
	//format:
	//0.000000000000000000e+00 +9.999999999999997780e-01 +3.927413949611491262e-15 
	
	fseek(fp, target_line * 77, SEEK_SET);
	fgets(row, 77, fp);
	//printf("Line %d: %s\n", target_line + 1, row);
	
	char *ptr = strtok(row, " ");
	current_ptr = ptr;
	//printf("current_ptr: %s\n", current_ptr);
	val = atof(current_ptr);
	*LensingAmplificationFactorArraySamplingFrequencies = val;
	//printf("val: %Lf, LF: %Lf\n", val, *LensingAmplificationFactorArraySamplingFrequencies);

	ptr = strtok(NULL, " ");
	current_ptr = ptr;
	val = atof(current_ptr);
	*LensingAmplificationFactorReal = val;

	ptr = strtok(NULL, " ");
	current_ptr = ptr;
	val = atof(current_ptr);
	*LensingAmplificationFactorComplex = val;
	fclose(fp);
	pthread_mutex_unlock(&file_mutex_FD);
}


double linear_interpolate_FD(long double x1, long double y1, long double x2, long double y2, long double x) {
	  // Check for degenerate cases: identical x values or x outside the range
	if (x1 == x2) {
		//printf("wtf");
		return y1; // Any y value works in this case
		} 
	else if (x < x1 || x > x2) {
		return NAN; // Indicate invalid input with NaN
		}
	  
	 // Calculate the slope of the line
	//printf("x1: %Lf\n y1: %Lf\n x2: %Lf\n y2: %Lf \n", x1, y1, x2, y2);
	//printf("x: %Lf \n", x);
	long double slope = (y2 - y1) / (x2 - x1);
	//printf("%Lf\n", slope);
	  
	// Use the point-slope form to interpolate y at x
	double y = y1 + slope * (x - x1);
	//printf("y = %f \n", y);
	  
	return y;
}




int XLALSimInspiralLensingAmplificationFactorFD(COMPLEX16FrequencySeries **tilde_h,
						LALDict *LALparams){
	
	int        i,j,k;
	int        target_line;
	int        count;
	double     real_part;
	double     complex_part;
	long double LensingAmplificationFactorReal;
	long double LensingAmplificationFactorComplex;
	long double LensingAmplificationFactorArraySamplingFrequencies;
	const char *LensingAmplificationFactorArray = XLALSimInspiralWaveformParamsLookupLensingAmplificationFactorArray(LALparams);
	COMPLEX16  LensingAmplificationFactor;


	printf("in XLALSimInspiralLensingAmplificationFactorFD\n");
	printf("test: %s \n", *LensingAmplificationFactorArray);
	if (*LensingAmplificationFactorArray == '\0'){
		printf("null \n");
	}
	else {
		printf("not null \n");
	}
	getchar();
	if (*LensingAmplificationFactorArray == '\0'){

		printf("Lensing Amplification factor is NULL.\n");

	}
	else{

		printf("Lensing Amplification factor array: %s \n", *LensingAmplificationFactorArray);

		if (strstr(*LensingAmplificationFactorArray, "default_identity_array")){

			for (k=0; k<(*tilde_h)->data->length; k++){

				real_part = 1;
				complex_part = 0;
				COMPLEX16 factor = real_part + complex_part * I;
				(*tilde_h)->data->data[k] *= factor;
			}
		}

		else{

			count = get_num_rows_FD(*LensingAmplificationFactorArray);
			printf("The count is %i \n", count);
			//initialize an array with 2 elements
			// two elements are used for interpolation
			printf("done initializing large array\n");
			XLALSimInspiralLensingAmplificationFactorReaderFD(*LensingAmplificationFactorArray,
					1, // read the second line of to obatain amplification factor deltaf 
					&LensingAmplificationFactorArraySamplingFrequencies,
					&LensingAmplificationFactorReal,
					&LensingAmplificationFactorComplex);
			long double delta_f_array = LensingAmplificationFactorArraySamplingFrequencies;	
			long double delta_f_waveform = (*tilde_h)->deltaF;
			printf("Frequency in waveform array is %Lf.\n", delta_f_waveform);
			printf("Frequency in amplificaiton factor array is %Lf.\n", LensingAmplificationFactorArraySamplingFrequencies);
			//getchar();
			
			if (delta_f_waveform < delta_f_array ){
				int ratio = (int) (delta_f_array / delta_f_waveform); // ratio for index allocation in amplification factor interpolation
				printf("waveform delta f is smaller than amplification factor delta f, now perform linear interpolation\n");
				printf("array delta f / waveform delta: %i \n", ratio);
				//getchar();
				
				for(k=0; k<(*tilde_h)->data->length; k++){
		
					if (k % ratio == 0 && k < count ){
						target_line = k / ratio;
						XLALSimInspiralLensingAmplificationFactorReaderFD(*LensingAmplificationFactorArray,
							target_line,
							&LensingAmplificationFactorArraySamplingFrequencies,
							&LensingAmplificationFactorReal,
							&LensingAmplificationFactorComplex);								
						real_part = LensingAmplificationFactorReal;
						complex_part = LensingAmplificationFactorComplex;
						LensingAmplificationFactor = real_part + complex_part * I;
						(*tilde_h)->data->data[k] *= LensingAmplificationFactor;
					}
					else{
						long double p = k / (delta_f_array / delta_f_waveform);
						long double LensingAmplificationFactorArraySamplingFrequencies1;
						long double LensingAmplificationFactorArraySamplingFrequencies2; 
						long double LensingAmplificationFactorReal1;
						long double LensingAmplificationFactorReal2;
						long double LensingAmplificationFactorComplex1;
						long double LensingAmplificationFactorComplex2;
	
						int target_line_1 = (int)floor(p);
						int target_line_2 = (int)ceil(p);
						printf("p: %Lf \n", p);
						printf("target line ceil: %i \n", target_line_2);
						printf("target line floor: %i \n", target_line_1);
						//getchar();

						XLALSimInspiralLensingAmplificationFactorReaderFD(*LensingAmplificationFactorArray,
							target_line_1,
							&LensingAmplificationFactorArraySamplingFrequencies1,
							&LensingAmplificationFactorReal1,
							&LensingAmplificationFactorComplex1);

						XLALSimInspiralLensingAmplificationFactorReaderFD(*LensingAmplificationFactorArray,
							target_line_2,
							&LensingAmplificationFactorArraySamplingFrequencies2,
							&LensingAmplificationFactorReal2,
							&LensingAmplificationFactorComplex2);

						real_part = linear_interpolate_FD(LensingAmplificationFactorArraySamplingFrequencies1,
									LensingAmplificationFactorReal1,
									LensingAmplificationFactorArraySamplingFrequencies2,
									LensingAmplificationFactorReal2,
									delta_f_waveform * k);

						complex_part = linear_interpolate_FD(LensingAmplificationFactorArraySamplingFrequencies1,
									LensingAmplificationFactorComplex1,
									LensingAmplificationFactorArraySamplingFrequencies2,
									LensingAmplificationFactorComplex2,
									delta_f_waveform * k);
						LensingAmplificationFactor = real_part + complex_part * I;
						(*tilde_h)->data->data[k] *= LensingAmplificationFactor;
						
					}
				}
			}

			else if (delta_f_waveform >= delta_f_array){
				int ratio = (int) (delta_f_waveform / delta_f_array); // ratio for amplification factor array index allocation
				for(k=0; k<(*tilde_h)->data->length; k++){
					//printf("ratio: %i\n", ratio);
					//printf("k: %i\n", k);
					target_line = (int) (k * ratio);
					//printf("read line %i\n", target_line);
					
					XLALSimInspiralLensingAmplificationFactorReaderFD(*LensingAmplificationFactorArray,
					target_line,
					&LensingAmplificationFactorArraySamplingFrequencies,
					&LensingAmplificationFactorReal,
					&LensingAmplificationFactorComplex);

					real_part = LensingAmplificationFactorReal;
					complex_part = LensingAmplificationFactorComplex;
					LensingAmplificationFactor = real_part + complex_part * I;
					//if (k<3){
					//	printf("Frequency in waveform array is %Lf.\n", delta_f_waveform * k);
					//	printf("Frequency in amplificaiton factor array is %Lf.\n", LensingAmplificationFactorArraySamplingFrequencies);
					//	printf("Amplication factor is %f + i%f\n", creal(LensingAmplificationFactor), cimag(LensingAmplificationFactor));
					//}
					(*tilde_h)->data->data[k] *= LensingAmplificationFactor;			
					//if ( k<3){
					//printf("New h_tilde(f=%f)=%e+i%e.\n", tilde_h->deltaF * k + tilde_h->f0, creal(tilde_h->data->data[k]), cimag(tilde_h->data->data[k]));
					//}
				}	
			}
		}
	}
}







/*
 * Here we assume that when the function is being called, it will receive a COMPLEXFREQUENCY16SERIES object,
 * depending on the types of image the injection it, we apply an extra Morse Phase factor onto the waveform.
*/

int XLALSimInspiralLensingImageTypeFD(COMPLEX16FrequencySeries **tilde_h, LALDict *LALparams){
	if( !LALparams ) {
		XLAL_ERROR(XLAL_EDOM);
	}
	if( !( tilde_h ) ) {
		XLAL_ERROR(XLAL_EDOM);
	}
	unsigned i, j, k;
	int placeholder=0;
	COMPLEX16 morsephasefactor;
	double true_deltaT = 1./4096;
	int LensingImageType = XLALSimInspiralWaveformParamsLookupLensingImageType(LALparams);
	COMPLEX16 LensingAmplificationFactor;
	int amp = 0 ; // Whether or not there is a amplification factor array
	long long int count = 1;
	double p;

	// Precalculated frequency domain modulation array
	//XLALSimInspiralLensingAmplificationFactorFD(tilde_h, LALparams);

	if (LensingImageType == 1) {
		placeholder += 1;
	} 
	else if (LensingImageType == 2) {
		for(j = 0; j < (*tilde_h)->data->length; j++) {
    			const double f = (*tilde_h)->f0 + j * (*tilde_h)->deltaF;
    			morsephasefactor = cexp(-0.5 * I * M_PI * f * true_deltaT);
			(*tilde_h)->data->data[j] *= -I;
		}
	/*	if((*tilde_h)->f0 == 0.0){
			(*tilde_h)->data->data[0] = cabs((*tilde_h)->data->data[0]);
			(*tilde_h)->data->data[(*tilde_h)->data->length - 1] = creal((*tilde_h)->data->data[(*tilde_h)->data->length - 1]);
		} */
	} 
	else if (LensingImageType == 3) {
		for(j = 0; j < (*tilde_h)->data->length; j++) {
			const double f = (*tilde_h)->f0 + j * (*tilde_h)->deltaF;
			morsephasefactor = cexp(-1.0 * I * M_PI * f * true_deltaT);
			(*tilde_h)->data->data[j] *= -1.;
		}
	/*	if((*tilde_h)->f0 == 0.0){
			(*tilde_h)->data->data[0] = cabs((*tilde_h)->data->data[0]);
			(*tilde_h)->data->data[(*tilde_h)->data->length - 1] = creal((*tilde_h)->data->data[(*tilde_h)->data->length - 1]);
		} */
	} 
	else {
		XLAL_ERROR(XLAL_EFUNC);
	}
	printf("The end of XLALSimInspiralLensingImageTypeFD\n");
	return XLAL_SUCCESS;
}

/** @} */

#if 0
#include <lal/PrintFTSeries.h>
#include <lal/PrintFTSeries.h>
int main(void)
{
	LIGOTimeGPS tc = { 888888888, 222222222 };
	REAL8 phic = 1.0;
	REAL8 deltaT = 1.0/16384.0;
	REAL8 m1 = 1.4*LAL_MSUN_SI;
	REAL8 m2 = 1.4*LAL_MSUN_SI;
	REAL8 r = 1e6*LAL_PC_SI;
	REAL8 i = 0.5*LAL_PI;
	REAL8 f_min = 100.0;
	REAL8 fRef = 0.;
	int O = -1;
	REAL8TimeSeries *hplus;
	REAL8TimeSeries *hcross;
	XLALSimInspiralEccentricTDPNRestricted(&hplus, &hcross, &tc, phic, deltaT, m1, m2, f_min, fRef, r, i, e_min, O);
	LALDPrintTimeSeries(hplus, "hp.dat");
	LALDPrintTimeSeries(hcross, "hc.dat");
	XLALDestroyREAL8TimeSeries(hplus);
	XLALDestroyREAL8TimeSeries(hcross);
	LALCheckMemoryLeaks();
	return 0;
}
#endif
