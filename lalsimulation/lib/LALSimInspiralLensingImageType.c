/*
 * Copyright (C) 2015 M. Haney, A. Gopakumar
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with with program; see the file COPYING. If not, write to the
 *  Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *  MA  02110-1301  USA
 */

#include <math.h>

#include <gsl/gsl_const.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_odeiv.h>
#include <lal/LALSimInspiralLensingImageType.h>
#include <lal/FrequencySeries.h>
#include <lal/LALSimInspiral.h>
#include <lal/LALConstants.h>
#include <lal/LALStdlib.h>
#include <lal/TimeFreqFFT.h>
#include <lal/TimeSeries.h>
#include <lal/Units.h>
#include <lal/XLALError.h>
#include <lal/LALSimInspiralWaveformParams.h>
#include <complex.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "check_series_macros.h"
#ifdef LAL_PTHREAD_LOCK
#include <pthread.h>
#endif
#ifdef __GNUC__
#define UNUSED __attribute__ ((unused))
#else
#define UNUSED
#endif

pthread_mutex_t file_mutex = PTHREAD_MUTEX_INITIALIZER; // Global mutex
//
//#ifdef LAL_PTHREAD_LOCK
//static pthread_once_t amplification_file_is_initialized = PTHREAD_ONCE_INIT;
//#endif


/*
 * Here we assume that when the function is being called, it will receive a TIMESERIES object,
 * we first need to Fourier transform it into the frequency domain. Then, depending on the 
 * types of image the injection it, we apply an extra Morse Phase factor onto the waveform.
*/

static unsigned long round_up_to_power_of_two(unsigned long x)
{
	unsigned n;
	x--;
	/* if x shares bits with x + 1, then x + 1 is not a power of 2 */
	for(n = 1; n && (x & (x + 1)); n *= 2)
		x |= x >> n;
	return x + 1;
}

int get_num_rows(const char *filename){
	printf("in get num row\n");
	printf("%s \n", filename);
	//pthread_mutex_lock(&file_mutex);
	FILE *fp = fopen(filename, "r");
	char c;
	long long int count = 0;
	printf("%i \n", count);
	for (c = getc(fp); c != EOF; c = getc(fp)){
		if (c == '\n'){
			count = count + 1;
		}
	}
	fclose(fp);
	//pthread_mutex_unlock(&file_mutex);
	return count;
}
//void testing(){

//	printf("testing\n");

//}



//Function for reading the amplification factor array
//array is being read at each wavoform frequency instead of reading the whole array
//in order to avoid overflow issues in C and HTCondor 
void XLALSimInspiralLensingAmplificationFactorReader(const char *filename, 
						int target_line,
						long double *LensingAmplificationFactorArraySamplingFrequencies,
						long double *LensingAmplificationFactorReal, 
						long double *LensingAmplificationFactorComplex){

	// Obtain the mutex before file operations
	pthread_mutex_lock(&file_mutex);

	FILE *fp = fopen(filename, "r");
	long double val;
	char row[77]; //numer of characters of the target line
	char *current_ptr;

	//Calcuclate the position of the line
	//Each line consist 77 characters
	//Please make sure the lensing amplfication array satisify:
	//deltaf 1.5625e-2
	//fmin = 0, fmax = 8192
	//format:
	//0.000000000000000000e+00 +9.999999999999997780e-01 +3.927413949611491262e-15 

	//printf("in XLALSimInspiralLensingAmplificationFactorReader \n");
	//printf("filename %s \n", filename);
	//printf("target_line: %i \n", target_line);
	fseek(fp, target_line * 77, SEEK_SET);
	fgets(row, 77, fp);
	//printf("Line %d: %s\n", target_line + 1, row);
	
	char *ptr = strtok(row, " ");
	current_ptr = ptr;
	//printf("current_ptr: %s\n", current_ptr);
	val = atof(current_ptr);
	*LensingAmplificationFactorArraySamplingFrequencies = val;
	//printf("val: %Lf, LF: %Lf\n", val, *LensingAmplificationFactorArraySamplingFrequencies);

	ptr = strtok(NULL, " ");
	current_ptr = ptr;
	val = atof(current_ptr);
	*LensingAmplificationFactorReal = val;

	ptr = strtok(NULL, " ");
	current_ptr = ptr;
	val = atof(current_ptr);
	*LensingAmplificationFactorComplex = val;

	//abandoned version of reader that overflow a lot
	/*
	for (i=0; i<count; i++){
		fgets ( row, 1000, temp);
		//puts(row);//
		char *current_ptr;
		char *ptr = strtok(row, " ");
		current_ptr = ptr;
		val = atof(current_ptr);
		//printf("Frequency : %Lf ; \n", val);
		LensingAmplificationFactorArraySamplingFrequencies[i] = val;
		ptr = strtok(NULL, " ");
		current_ptr = ptr;
		val = atof(current_ptr);
		//printf("Real : %Lf ; ", val);//
		LensingAmplificationFactorReal[i] = val;
		ptr = strtok(NULL, " ");
		current_ptr = ptr;
		val = atof(current_ptr);
		//printf("Complex : %Lf \n ", val);//
		LensingAmplificationFactorComplex[i] = val;
		//printf("%i \n", i);
	}
	*/
	fclose(fp);
	pthread_mutex_unlock(&file_mutex);
}

double linear_interpolate(long double x1,
			long double y1,
			long double x2,
			long double y2,
			long double x) {

	  // Check for degenerate cases: identical x values or x outside the range
	if (x1 == x2) {
		//printf("wtf");
		return y1; // Any y value works in this case
		} 
	else if (x < x1 || x > x2) {
		return NAN; // Indicate invalid input with NaN
		}
	  
	 // Calculate the slope of the line
	//printf("x1: %Lf\n y1: %Lf\n x2: %Lf\n y2: %Lf \n", x1, y1, x2, y2);
	//printf("x: %Lf \n", x);
	long double slope = (y2 - y1) / (x2 - x1);
	//printf("%Lf\n", slope);
	  
	// Use the point-slope form to interpolate y at x
	double y = y1 + slope * (x - x1);
	//printf("y = %f \n", y);
	  
	return y;
}


const char* get_formatted_string(const char *input_string) {
	printf("in get formatted_string \n");
	//printf("input string: %s \n", input_string);
	if (input_string == NULL) {
		// Input string is empty or null
		//printf("case1\n");
		return NULL; // Indicate empty string
		}	
	else if (strlen(input_string) > 0 && input_string != NULL){
		//printf("case2\n");
		printf("%s \n", input_string);
		return input_string;
		}
	else {
		//printf("case3\n");
		return NULL;
		}
	}



int XLALSimInspiralLensingAmplificationFactor(COMPLEX16FrequencySeries *tilde_h,
						LALDict *LALparams){
	//pthread_mutex_lock(&file_mutex);
	int        i,j,k;
	int        target_line;
	//FIXME hardcoded the number of lines in the amplification array here, update get num row later
	int        count = 262145;
	double     real_part;
	double     complex_part;
	long double LensingAmplificationFactorReal;
	long double LensingAmplificationFactorComplex;
	long double LensingAmplificationFactorArraySamplingFrequencies;
	//const char *LensingAmplificationFactorArray_raw = XLALSimInspiralWaveformParamsLookupLensingAmplificationFactorArray(LALparams);
	printf("before formatted string \n");
	const char *LensingAmplificationFactorArray = get_formatted_string(XLALSimInspiralWaveformParamsLookupLensingAmplificationFactorArray(LALparams));
	COMPLEX16  LensingAmplificationFactor;

	printf("in XLALSimInspiralLensingAmplificationFactor \n");	
	
	//printf("testing: %s \n", LensingAmplificationFactorArray_raw);
	//printf("LensingAmplificationFactorArray_formatted: %s \n", LensingAmplificationFactorArray);
	
	//(*strain) = resized_strain;
	REAL8 m1 = XLALSimInspiralWaveformParamsLookupMass1(LALparams);
	REAL8 m2 = XLALSimInspiralWaveformParamsLookupMass2(LALparams);
	REAL8 S1z = XLALSimInspiralWaveformParamsLookupSpin1z(LALparams);
	REAL8 S2z = XLALSimInspiralWaveformParamsLookupSpin2z(LALparams);
	REAL8 f_min = XLALSimInspiralWaveformParamsLookupF22Start(LALparams);
	REAL8 deltaT = XLALSimInspiralWaveformParamsLookupDeltaT(LALparams);

	size_t end;
	//const double extra_cycles = 3.0; /* more extra time measured in cycles at the starting frequency */
	const double extra_time_fraction = 0.1; /* fraction of waveform duration to add as extra time for tapering */
	double tshift, textra, tmerge, tchirp;
	double fisco, fstart;
	double s;

	/* if the requested low frequency is below the lowest Kerr ISCO
	 *      * frequency then change it to that frequency */


	//printf("f_isco (Fisco): %f \n", fisco);
	//printf("f_min (F22 start frequency): %lf \n", f_min);
	//getchar();
	
	

	if (LensingAmplificationFactorArray == '\0'){

		printf("Lensing Amplification factor is NULL.\n");

	}
	else{

		printf("Lensing Amplification factor array: %s \n", LensingAmplificationFactorArray);

		if (strstr(LensingAmplificationFactorArray, "default_identity_array")){

			for (k=0; k<tilde_h->data->length; k++){

				real_part = 1;
				complex_part = 0;
				COMPLEX16 factor = real_part + complex_part * I;
				tilde_h->data->data[k] *= factor;
			}
		}

		else{

			//count = get_num_rows(LensingAmplificationFactorArray);
			printf("The count is %i \n", count);
			//initialize an array with 2 elements
			// two elements are used for interpolation
			//printf("done initializing large array\n");
			XLALSimInspiralLensingAmplificationFactorReader(LensingAmplificationFactorArray,
					1, // read the second line of to obatain amplification factor deltaf 
					&LensingAmplificationFactorArraySamplingFrequencies,
					&LensingAmplificationFactorReal,
					&LensingAmplificationFactorComplex);
			//printf("get_array complete\n");	
			long double delta_f_array = LensingAmplificationFactorArraySamplingFrequencies;	
			long double delta_f_waveform = tilde_h->deltaF;
			printf("deltaf in waveform array is %Lf.\n", delta_f_waveform);
			printf("deltaf in amplificaiton factor array is %Lf.\n", LensingAmplificationFactorArraySamplingFrequencies);
			//getchar();
			
			if (delta_f_waveform < delta_f_array ){
				int ratio = (int) (delta_f_array / delta_f_waveform); // ratio for index allocation in amplification factor interpolation
				printf("waveform delta f is smaller than amplification factor delta f, now interpolate\n");
				printf("array delta f / waveform delta: %i \n", ratio);
				//getchar();
				
				for(k=0; k<tilde_h->data->length; k++){
		
					if (k % ratio == 0 && k < count ){
						target_line = k / ratio;
						XLALSimInspiralLensingAmplificationFactorReader(LensingAmplificationFactorArray,
							target_line,
							&LensingAmplificationFactorArraySamplingFrequencies,
							&LensingAmplificationFactorReal,
							&LensingAmplificationFactorComplex);								
						real_part = LensingAmplificationFactorReal;
						complex_part = LensingAmplificationFactorComplex;
						LensingAmplificationFactor = real_part + complex_part * I;
						tilde_h->data->data[k] *= LensingAmplificationFactor;
					}
					else{
						long double p = k / (delta_f_array / delta_f_waveform);
						long double LensingAmplificationFactorArraySamplingFrequencies1;
						long double LensingAmplificationFactorArraySamplingFrequencies2; 
						long double LensingAmplificationFactorReal1;
						long double LensingAmplificationFactorReal2;
						long double LensingAmplificationFactorComplex1;
						long double LensingAmplificationFactorComplex2;
	
						int target_line_1 = (int)floor(p);
						int target_line_2 = (int)ceil(p);
						printf("p: %Lf \n", p);
						printf("target line ceil: %i \n", target_line_2);
						printf("target line floor: %i \n", target_line_1);
						//getchar();

						XLALSimInspiralLensingAmplificationFactorReader(LensingAmplificationFactorArray,
							target_line_1,
							&LensingAmplificationFactorArraySamplingFrequencies1,
							&LensingAmplificationFactorReal1,
							&LensingAmplificationFactorComplex1);

						XLALSimInspiralLensingAmplificationFactorReader(LensingAmplificationFactorArray,
							target_line_2,
							&LensingAmplificationFactorArraySamplingFrequencies2,
							&LensingAmplificationFactorReal2,
							&LensingAmplificationFactorComplex2);

						real_part = linear_interpolate(LensingAmplificationFactorArraySamplingFrequencies1,
									LensingAmplificationFactorReal1,
									LensingAmplificationFactorArraySamplingFrequencies2,
									LensingAmplificationFactorReal2,
									delta_f_waveform * k);

						complex_part = linear_interpolate(LensingAmplificationFactorArraySamplingFrequencies1,
									LensingAmplificationFactorComplex1,
									LensingAmplificationFactorArraySamplingFrequencies2,
									LensingAmplificationFactorComplex2,
									delta_f_waveform * k);
						LensingAmplificationFactor = real_part + complex_part * I;
						tilde_h->data->data[k] *= LensingAmplificationFactor;
						
					}
				}
			}

			else if (delta_f_waveform >= delta_f_array){
				int ratio = (int) (delta_f_waveform / delta_f_array); // ratio for amplification factor array index allocation
				for(k=0; k<tilde_h->data->length; k++){
						//printf("ratio: %i\n", ratio);
						//printf("k: %i\n", k);
					//target_line = (int) (k * ratio);
						//printf("read line %i\n", target_line);

					//XLALSimInspiralLensingAmplificationFactorReader(LensingAmplificationFactorArray,
					//target_line,
					//&LensingAmplificationFactorArraySamplingFrequencies,
					//&LensingAmplificationFactorReal,
					//&LensingAmplificationFactorComplex);
						
					//if ((delta_f_waveform*k) > XLALSimInspiralWaveformParamsLookupF22Start(LALparams) && (delta_f_waveform*k) <= 1024.0){
					if ((delta_f_waveform*k) <= 1024.0){
						target_line = (int) (k * ratio);
						XLALSimInspiralLensingAmplificationFactorReader(LensingAmplificationFactorArray,
						target_line,
						&LensingAmplificationFactorArraySamplingFrequencies,
						&LensingAmplificationFactorReal,
						&LensingAmplificationFactorComplex);


						if (LensingAmplificationFactorArraySamplingFrequencies != delta_f_waveform * k){
							printf("Frequency in waveform array is %Lf.\n", delta_f_waveform * k);
							printf("Frequency in amplificaiton factor array is %Lf.\n", LensingAmplificationFactorArraySamplingFrequencies);
							printf("they don't match\n");
							XLAL_ERROR(XLAL_EFUNC);
						}

						//printf("waveform frequency: %Lf\n", (delta_f_waveform*k));
						//getchar();
						real_part = LensingAmplificationFactorReal;
						complex_part = LensingAmplificationFactorComplex;
						LensingAmplificationFactor = real_part + complex_part * I;
						//printf("Amplication factor is %f + i%f\n", creal(LensingAmplificationFactor), cimag(LensingAmplificationFactor));
					}
					else {
						//printf("identical factor applied\n");
						LensingAmplificationFactor = 1.0 + 0.0 * I	;
						//printf("Amplication factor is %f + i%f\n", creal(LensingAmplificationFactor), cimag(LensingAmplificationFactor));
					}

					//if (k < 3){
						//printf("Frequency in waveform array is %Lf.\n", delta_f_waveform * k);
						//printf("Frequency in amplificaiton factor array is %Lf.\n", LensingAmplificationFactorArraySamplingFrequencies);
						//printf("original waveform amplitude: %e+%ei. \n", creal(tilde_h->data->data[k]),  cimag(tilde_h->data->data[k]));
						//printf("Amplication factor is %f + i%f\n", creal(LensingAmplificationFactor), cimag(LensingAmplificationFactor));
					//}
					tilde_h->data->data[k] *= LensingAmplificationFactor;			
					//if (k < 3){
					//printf("New h_tilde(f=%f)=%e+%ei.\n", tilde_h->deltaF * k + tilde_h->f0, creal(tilde_h->data->data[k]), cimag(tilde_h->data->data[k]));
					//}
				}	
			}
		}
	}
	//pthread_mutex_unlock(&file_mutex);
}



int XLALSimInspiralLensingImageType(REAL8TimeSeries **strain,
				LALDict *LALparams){
	//pthread_mutex_lock(&file_mutex);
	unsigned long sampling_freq;
	sampling_freq = (*strain)->data->length * (*strain)->deltaT;
	sampling_freq = round_up_to_power_of_two(sampling_freq);
	unsigned long new_data_length = sampling_freq / (*strain)->deltaT;
	REAL8TimeSeries *resized_strain;
	REAL8TimeSeries *clean_strain;
	resized_strain = XLALCutREAL8TimeSeries((*strain), 0, (*strain)->data->length);
	resized_strain = XLALResizeREAL8TimeSeries(resized_strain, 0, new_data_length);
	(*strain) = resized_strain;
	COMPLEX16FrequencySeries *tilde_h;
	REAL8FFTPlan *plan;

	size_t i,l,j,k;
	double p ;
	int length;
	const double extra_cycles = 3.0;
	const double extra_time_fraction = 0.1; 
	int placeholder = 0;
	double true_deltaT;
	double test1;
	double current_f;
	double deltaF;
	long long int count = 1;
	int amp = 0; // Whether or not there is a amplification factor array
	int LensingImageType = XLALSimInspiralWaveformParamsLookupLensingImageType(LALparams);
	REAL8 f_min = XLALSimInspiralWaveformParamsLookupF22Start(LALparams);
	REAL8 meanPerAno = XLALSimInspiralWaveformParamsLookupMeanPerAno(LALparams);
	REAL8 deltaT = XLALSimInspiralWaveformParamsLookupDeltaT(LALparams);
	REAL8 f_ref = XLALSimInspiralWaveformParamsLookupF22Ref(LALparams);
	REAL8 m1 = XLALSimInspiralWaveformParamsLookupMass1(LALparams);
	REAL8 m2 = XLALSimInspiralWaveformParamsLookupMass2(LALparams);
	REAL8 distance = XLALSimInspiralWaveformParamsLookupDistance(LALparams);
	REAL8 S1z = XLALSimInspiralWaveformParamsLookupSpin1z(LALparams);
	REAL8 S2z = XLALSimInspiralWaveformParamsLookupSpin2z(LALparams);

	double original_f_min = f_min; /* f_min might be overwritten below, so keep original value */
	double fstart;
	double tchirp, s;
	size_t end; //, chirplen;
	double tmerge, textra;
	double fisco;
	double tshift;
    COMPLEX16FrequencySeries *hptilde = NULL;
    COMPLEX16FrequencySeries *hctilde = NULL;

	
    

	//FFT the TD waveform array
	true_deltaT = (*strain)->deltaT;
	tilde_h = XLALCreateCOMPLEX16FrequencySeries(NULL, &(*strain)->epoch, 0.0, 0.0, &lalDimensionlessUnit, (*strain)->data->length / 2.0 + 1.0);
	plan = XLALCreateForwardREAL8FFTPlan((*strain)->data->length, 1);
	if(!tilde_h || !plan) {
		XLALDestroyCOMPLEX16FrequencySeries(tilde_h);
		XLALDestroyREAL8FFTPlan(plan);
		XLAL_ERROR(XLAL_EFUNC);
	}
	i = XLALREAL8TimeFreqFFT(tilde_h, (*strain), plan);
	XLALDestroyREAL8FFTPlan(plan);

	if(i) {
		XLALDestroyCOMPLEX16FrequencySeries(tilde_h);
		XLAL_ERROR(XLAL_EFUNC);
	}
	//printf("in XLALSimInspiralLensingImageType\n");
	// Precalculated frequency domain modulation array
	XLALSimInspiralLensingAmplificationFactor(tilde_h, LALparams);
	//printf("after amplification factor\n");

	if(LensingImageType == 1){
		printf("Lensing image type is 1.\n");
		/*for(j = 0; j < tilde_h->data->length; k++){
			const double f = tilde_h->f0 + k * tilde_h->deltaF;
			morsephasefactor = cexp(0.0 * I * M_PI * f * true_deltaT);
			printf("The morse phase factor is %f + %f i.\n", creal(morsephasefactor), cimag(morsephasefactor));
			tilde_h->data->data[j] *= morsephasefactor;
		}
		*/
		if(tilde_h->f0 == 0.0){
			tilde_h->data->data[0] = cabs(tilde_h->data->data[0]);
			tilde_h->data->data[tilde_h->data->length - 1] = creal(tilde_h->data->data[tilde_h->data->length - 1]);
		}
		placeholder += 1;
	}
	else if(LensingImageType == 2){
		printf("Lensing image type is 2.\n");
		int m;
		for(m = 0; m < tilde_h->data->length; m++){
			double f = tilde_h->f0 + m * tilde_h->deltaF;
			COMPLEX16 morsephasefactor = cexp(-0.5 * I * M_PI * f * true_deltaT);
			//printf("The morse phase factor is %f + %f i.\n", creal(morsephasefactor), cimag(morsephasefactor));
			tilde_h->data->data[m] *= morsephasefactor;
		}
		if(tilde_h->f0 == 0.0){
			tilde_h->data->data[0] = cabs(tilde_h->data->data[0]);
			tilde_h->data->data[tilde_h->data->length - 1] = creal(tilde_h->data->data[tilde_h->data->length - 1]);	
		}
	}
	else if(LensingImageType == 3){
		printf("Lensing image type is 3.\n");
		int m;
		for (m = 0; m < tilde_h->data->length; m++){
			double f = tilde_h->f0 + m * tilde_h->deltaF;
			printf("%f \n", f);
			COMPLEX16 morsephasefactor = cexp(-I * M_PI * f * true_deltaT);
			printf("%f \n", morsephasefactor);
			tilde_h->data->data[m] *= morsephasefactor;
		}
		if(tilde_h->f0 == 0){
			tilde_h->data->data[0] = cabs(tilde_h->data->data[0]);
			tilde_h->data->data[tilde_h->data->length - 1] = creal(tilde_h->data->data[tilde_h->data->length - 1]);
		}
	}
	else{
		XLAL_ERROR(XLAL_EERR);
	}

    /* adjust the reference frequency for certain precessing approximants:
     * if that approximate interprets f_ref==0 to be f_min, set f_ref=f_min;
     * otherwise do nothing */
    //FIX_REFERENCE_FREQUENCY(f_ref, f_min, approximant);

    /* apply redshift correction to dimensionful source-frame quantities */
    REAL8 z=XLALSimInspiralWaveformParamsLookupRedshift(LALparams);
    if (z != 0.0) {
        m1 *= (1.0 + z);
        m2 *= (1.0 + z);
        distance *= (1.0 + z);  /* change from comoving (transverse) distance to luminosity distance */
    }
    /* set redshift to zero so we don't accidentally apply it again later */
    z=0.;
	printf("pass1\n");
    if (LALparams)
      XLALSimInspiralWaveformParamsInsertRedshift(LALparams,z);

    /* if the requested low frequency is below the lowest Kerr ISCO
     * frequency then change it to that frequency */
    //fisco = 1.0 / (pow(9.0, 1.5) * LAL_PI * (m1 + m2) * LAL_MTSUN_SI / LAL_MSUN_SI);
    //if (f_min > fisco)
     //   f_min = fisco;

    /* upper bound on the chirp time starting at f_min */
    //tchirp = XLALSimInspiralChirpTimeBound(f_min, m1, m2, S1z, S2z);

    /* upper bound on the final black hole spin */
    //s = XLALSimInspiralFinalBlackHoleSpinBound(S1z, S2z);

    /* upper bound on the final plunge, merger, and ringdown time */
    //tmerge = XLALSimInspiralMergeTimeBound(m1, m2) + XLALSimInspiralRingdownTimeBound(m1 + m2, s);

    /* extra time to include for all waveforms to take care of situations
     * where the frequency is close to merger (and is sweeping rapidly):
     * this is a few cycles at the low frequency */
    //textra = extra_cycles / f_min;

    /* generate the conditioned waveform in the frequency domain */
    /* note: redshift factor has already been applied above */
    /* set deltaF = 0 to get a small enough resolution */
    //retval = XLALSimInspiralFD(&hptilde, &hctilde, m1, m2, S1x, S1y, S1z, S2x, S2y, S2z, distance, inclination, phiRef, longAscNodes, eccentricity, meanPerAno, 0.0, f_min, f_max, f_ref, LALparams, approximant);
    //if (retval < 0)
    //    XLAL_ERROR(XLAL_EFUNC);

    /* we want to make sure that this waveform will give something
     * sensible if it is later transformed into the time domain:
     * to avoid the end of the waveform wrapping around to the beginning,
     * we shift waveform backwards in time and compensate for this
     * shift by adjusting the epoch -- note that XLALSimInspiralFD
     * guarantees that there is extra padding to do this */
	//printf("pass2\n");
    //tshift = round(textra / deltaT) * deltaT; /* integer number of samples */
	//printf("pass2\n"); // tilde_h->data->length
    //for (k = 0; k < tilde_h->data->length; ++k) {
     //   double complex phasefac = cexp(2.0 * M_PI * I * k * tilde_h->deltaF * tshift);
      //  tilde_h->data->data[k] *= phasefac;
        //tilde_h->data->data[k] *= phasefac;
    //}
	//printf("pass2\n");
    //XLALGPSAdd(&tilde_h->epoch, tshift);
	//printf("pass2\n");
    //XLALGPSAdd(tilde_h->epoch, tshift);
	//printf("pass3\n");
    /* transform the waveform into the time domain */
    //chirplen = 2 * (tilde_h->data->length - 1);
	//REAL8TimeSeries hplus;
	//REAL8TimeSeries hplus;
	//REAL8TimeSeries hcross;
	//REAL8TimeSeries *hplus = NULL, *hcross = NULL;
	//REAL8TimeSeries hcross;
    //hplus = XLALCreateREAL8TimeSeries("H_PLUS", &tilde_h->epoch, 0.0, deltaT, &lalStrainUnit, chirplen);
    //hcross = XLALCreateREAL8TimeSeries("H_CROSS", &tilde_h->epoch, 0.0, deltaT, &lalStrainUnit, chirplen);
	//printf("pass3\n");
    //plan = XLALCreateReverseREAL8FFTPlan(chirplen, 0);
    //if (!(hplus) || !(hcross) || !plan) {
     //   XLALDestroyCOMPLEX16FrequencySeries(tilde_h);
        //XLALDestroyCOMPLEX16FrequencySeries(hctilde);
      //  XLALDestroyREAL8TimeSeries(hcross);
      //  XLALDestroyREAL8TimeSeries(hplus);
       // XLALDestroyREAL8FFTPlan(plan);
       // XLAL_ERROR(XLAL_EFUNC);
    //}
    //XLALREAL8FreqTimeFFT((*strain), tilde_h, plan);
    //XLALREAL8FreqTimeFFT(hcross, tilde_h, plan);
	printf("pass4\n");
    /* apply time domain filter at original f_min */
	//printf("%lf \n", original_f_min);
    //XLALHighPassREAL8TimeSeries((*strain), original_f_min, 0.99, 8);
    //XLALHighPassREAL8TimeSeries(hcross, original_f_min, 0.99, 8);
	printf("pass5\n");
    /* compute how long a chirp we should have */
    /* revised estimate of chirp length from new start frequency */
    //fstart = XLALSimInspiralChirpStartFrequencyBound((1.0 + extra_time_fraction) * tchirp, m1, m2);
    //tchirp = XLALSimInspiralChirpTimeBound(fstart, m1, m2, S1z, S2z);
	printf("pass6\n");



    /* total expected chirp length includes merger */
    //chirplen = round((tchirp + tmerge) / deltaT ); //extra emperical factor for lensing since the error would be magnified
	//printf("%zu \n", chirplen);

    /* amount to snip off at the end is tshift */
    //end = (*strain)->data->length - round(tshift / deltaT);
	//printf("%zu \n", end);

    /* snip off extra time at beginning and at the end */
    //XLALResizeREAL8TimeSeries((*strain), end - chirplen, chirplen);
    //XLALResizeREAL8TimeSeries(hcross, end - chirplen, chirplen);
	//printf("pass7\n");
    /* clean up */
    //XLALDestroyREAL8FFTPlan(plan);
    //XLALDestroyCOMPLEX16FrequencySeries(hptilde);
    //XLALDestroyCOMPLEX16FrequencySeries(tilde_h);
	//printf("pass8\n");
    /* final tapering at the beginning and at the end to remove filter transients */	
    /* waveform should terminate at a frequency >= Schwarzschild ISCO
     * so taper one cycle at this frequency at the end; should not make
     * any difference to IMR waveforms */
    //fisco = 1.0 / (pow(6.0, 1.5) * LAL_PI * (m1 + m2) * LAL_MTSUN_SI / LAL_MSUN_SI);
    //XLALSimInspiralTDConditionStage2((*strain), (*strain), f_min, fisco);
	//printf("pass9\n");
	//printf("pass10\n");
	size_t chirplen = 2 * (tilde_h->data->length - 1);
	//chirplen = 2 * (tilde_h->data->length - 1);
	plan = XLALCreateReverseREAL8FFTPlan(chirplen, 1);	
	printf("pass9\n");
	if(!plan) {
		XLALDestroyCOMPLEX16FrequencySeries(tilde_h);
		XLAL_ERROR(XLAL_EFUNC);
	}
	printf("pass10\n");
	l = XLALREAL8FreqTimeFFT((*strain), tilde_h, plan);
	//printf("pass11\n");
	//(*strain) = resized_strain;
	//REAL8 m1 = XLALSimInspiralWaveformParamsLookupMass1(LALparams);
	//REAL8 m2 = XLALSimInspiralWaveformParamsLookupMass2(LALparams);
	//REAL8 S1z = XLALSimInspiralWaveformParamsLookupSpin1z(LALparams);
	//REAL8 S2z = XLALSimInspiralWaveformParamsLookupSpin2z(LALparams);
	//REAL8 f_min = XLALSimInspiralWaveformParamsLookupF22Start(LALparams);
	//REAL8 deltaT = XLALSimInspiralWaveformParamsLookupDeltaT(LALparams);

	//size_t end;
	//const double extra_cycles = 3.0; /* more extra time measured in cycles at the starting frequency */
	//const double extra_time_fraction = 0.1; /* fraction of waveform duration to add as extra time for tapering */
	//double tshift, textra, tmerge, tchirp;
	//double fisco, fstart;
	//double s;
	
	/* if the requested low frequency is below the lowest Kerr ISCO
	 *      * frequency then change it to that frequency */

	//fisco = 1.0 / (pow(9.0, 1.5) * LAL_PI * (m1 + m2) * LAL_MTSUN_SI / LAL_MSUN_SI);
	//printf("pass10");
	//if (f_min > fisco)
	//  f_min = fisco;

	/* upper bound on the chirp time starting at f_min */
	//tchirp = XLALSimInspiralChirpTimeBound(f_min, m1, m2, S1z, S2z);

	/* upper bound on the final black hole spin */
	//s = XLALSimInspiralFinalBlackHoleSpinBound(S1z, S2z);

	/* upper bound on the final plunge, merger, and ringdown time */
	//tmerge = XLALSimInspiralMergeTimeBound(m1, m2) + XLALSimInspiralRingdownTimeBound(m1 + m2, s);

	/* extra time to include for all waveforms to take care of situations
	 *      * where the frequency is close to merger (and is sweeping rapidly):
	 *           * this is a few cycles at the low frequency */
	//textra = extra_cycles / f_min;

	//tshift = round(textra / deltaT) * deltaT; /* integer number of samples */
	//printf("f_min: %f\n", f_min);
		

	/* apply time domain filter at original f_min */
	//XLALHighPassREAL8TimeSeries(*strain, f_min, 0.99, 8);
	//XLALHighPassREAL8TimeSeries(*hcross, original_f_min, 0.99, 8);

	/* compute how long a chirp we should have */
	/* revised estimate of chirp length from new start frequency */
	//fstart = XLALSimInspiralChirpStartFrequencyBound((1.0 + extra_time_fraction) * tchirp, m1, m2);
	//tchirp = XLALSimInspiralChirpTimeBound(fstart, m1, m2, S1z, S2z);

	/* total expected chirp length includes merger */
	//chirplen = round((tchirp + tmerge) / deltaT);

	/* amount to snip off at the end is tshift */
	//end = (*strain)->data->length - round(tshift / deltaT);

	/* snip off extra time at beginning and at the end */
	//XLALResizeREAL8TimeSeries(*strain, end - chirplen, chirplen);

	XLALDestroyREAL8FFTPlan(plan);
	XLALDestroyCOMPLEX16FrequencySeries(tilde_h);
	if(l)
		XLAL_ERROR(XLAL_EFUNC);
	if(fabs((*strain)->deltaT - true_deltaT) / true_deltaT > 1e-12) {
		XLALPrintError("%s(): error: oops, internal sample rate mismatch\n", __func__);
		XLAL_ERROR(XLAL_EERR);
	}
	(*strain)->deltaT = true_deltaT;
	//pthread_mutex_unlock(&file_mutex);

	printf("The end of XLALSimInspiralLensingImageType\n");
	return XLAL_SUCCESS;
}


/** @} */

#if 0
#include <lal/PrintFTSeries.h>
#include <lal/PrintFTSeries.h>
int main(void)
{
	LIGOTimeGPS tc = { 888888888, 222222222 };
	REAL8 phic = 1.0;
	REAL8 deltaT = 1.0/16384.0;
	REAL8 m1 = 1.4*LAL_MSUN_SI;
	REAL8 m2 = 1.4*LAL_MSUN_SI;
	REAL8 r = 1e6*LAL_PC_SI;
	REAL8 i = 0.5*LAL_PI;
	REAL8 f_min = 100.0;
	REAL8 fRef = 0.;
	int O = -1;
	REAL8TimeSeries *hplus;
	REAL8TimeSeries *hcross;
	XLALSimInspiralEccentricTDPNRestricted(&hplus, &hcross, &tc, phic, deltaT, m1, m2, f_min, fRef, r, i, e_min, O);
	LALDPrintTimeSeries(hplus, "hp.dat");
	LALDPrintTimeSeries(hcross, "hc.dat");
	XLALDestroyREAL8TimeSeries(hplus);
	XLALDestroyREAL8TimeSeries(hcross);
	LALCheckMemoryLeaks();
	return 0;
}
#endif
